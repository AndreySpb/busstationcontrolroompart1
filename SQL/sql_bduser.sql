-- MySQL Script generated by MySQL Workbench
-- Вт 07 мар 2023 23:17:36
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`drivers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`drivers` ;

CREATE TABLE IF NOT EXISTS `mydb`.`drivers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `family` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `birthday` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`buses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`buses` ;

CREATE TABLE IF NOT EXISTS `mydb`.`buses` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `qnt_pas` INT NULL,
  `status` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`cities`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`cities` ;

CREATE TABLE IF NOT EXISTS `mydb`.`cities` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `citiescol` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`routs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`routs` ;

CREATE TABLE IF NOT EXISTS `mydb`.`routs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cities_id_in` INT NOT NULL,
  `cities_id_out` INT NOT NULL,
  `travel_time` VARCHAR(45) NULL,
  `length` FLOAT NULL,
  `price` FLOAT NULL,
  `del` INT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `cities_id_in`, `cities_id_out`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_routs_cities_idx` (`cities_id_in` ASC),
  INDEX `fk_routs_cities1_idx` (`cities_id_out` ASC),
  CONSTRAINT `fk_routs_cities`
    FOREIGN KEY (`cities_id_in`)
    REFERENCES `mydb`.`cities` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_routs_cities1`
    FOREIGN KEY (`cities_id_out`)
    REFERENCES `mydb`.`cities` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`trips`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`trips` ;

CREATE TABLE IF NOT EXISTS `mydb`.`trips` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `routs_id` INT NOT NULL,
  `drivers_id` INT NOT NULL,
  `buses_id` INT NOT NULL,
  `dt_str_trip` DATE NULL,
  `time_str_trip` VARCHAR(45) NULL,
  `del` INT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `routs_id`, `drivers_id`, `buses_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_trips_routs1_idx` (`routs_id` ASC),
  INDEX `fk_trips_drivers1_idx` (`drivers_id` ASC),
  INDEX `fk_trips_buses1_idx` (`buses_id` ASC),
  CONSTRAINT `fk_trips_routs1`
    FOREIGN KEY (`routs_id`)
    REFERENCES `mydb`.`routs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_trips_drivers1`
    FOREIGN KEY (`drivers_id`)
    REFERENCES `mydb`.`drivers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_trips_buses1`
    FOREIGN KEY (`buses_id`)
    REFERENCES `mydb`.`buses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`tickets_sale`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`tickets_sale` ;

CREATE TABLE IF NOT EXISTS `mydb`.`tickets_sale` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `trips_id` INT NOT NULL,
  `dt_sale` DATE NULL,
  `price` FLOAT NULL,
  `delete` INT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `trips_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_tickets_sale_trips1_idx` (`trips_id` ASC),
  CONSTRAINT `fk_tickets_sale_trips1`
    FOREIGN KEY (`trips_id`)
    REFERENCES `mydb`.`trips` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;